/*
Code du recepteur.  
Allume ou eteint des LEDs a partir des informations
reçues de l'emetteur.
 
 4 avril 2012.
 */

// les librairies necessaires au fonctionnement du nRF24L01
#include <SPI.h>
#include <Mirf.h>
#include <nRF24L01.h>
#include <MirfHardwareSpiDriver.h>
#include <Adafruit_NeoPixel.h>


// Which pin on the Arduino is connected to the NeoPixels?
// On a Trinket or Gemma we suggest changing this to 1
#define PIN            6

// How many NeoPixels are attached to the Arduino?

#define NUMPIXELS      80
// When we setup the NeoPixel library, we tell it how many pixels, and which pin to use to send signals.
// Note that for older NeoPixel strips you might need to change the third parameter--see the strandtest
// example for more information on possible values.
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

byte message;
// La sortie numerique 2 est branchee a la premiere LED (les autres sont adjacentes)
#define LED 6

void setup(){
  pixels.begin();
  Serial.begin(9600);
  Mirf.cePin = 8; // CE branchee sur 8
  Mirf.csnPin = 9; // CSN branchee sur 9
  Mirf.spi = &MirfHardwareSpi; 
  Mirf.init(); 

  Mirf.channel = 0; 
  Mirf.payload = sizeof(byte); // taille de ce qu'on va recevoir
  Mirf.config(); // Tout est bon ? Ok let's go !

  Mirf.setTADDR((byte *)"nrf01"); // on baptise les deux modules
  Mirf.setRADDR((byte *)"nrf02"); 

  // configuration des sorties (LEDs)
  for (int i=0; i <= 6; i++){
    pinMode(LED+i, OUTPUT);
  } 

}

void loop(){

 int i;

  byte data[Mirf.payload]; // data va contenir les donnees reçues
  
  if(!Mirf.isSending() && Mirf.dataReady()) { 

    Mirf.getData(data); // reception du message
    
  message = data[0];

   Serial.println(message);
   Serial.println("");
    
    // on allume ou eteint les LEDs

    if  (message == 3 ){
    // digitalWrite(LED+5, HIGH); // LED allumee

        for(i=0;i<NUMPIXELS;i++)
      {
      pixels.setPixelColor(i, pixels.Color(255,0,0)); // Moderately bright red color.
      pixels.show(); // This sends the updated pixel color to the hardware.
      }
      

    }   
    
    if (message== 4){
    //  digitalWrite(LED+4, HIGH); // LED allumee
       for(i=0;i<NUMPIXELS;i++)
      {
      pixels.setPixelColor(i, pixels.Color(0,0,255)); // Moderately bright blue color.
      pixels.show(); // This sends the updated pixel color to the hardware.
      }
      
      
    }

     if (message== 5 ){
    //  digitalWrite(LED+3, HIGH); // LED allumee

       for(i=0;i<NUMPIXELS;i++)
      {
      pixels.setPixelColor(i, pixels.Color(0,255,0)); // Moderately bright green color.
      pixels.show(); // This sends the updated pixel color to the hardware.
      }
      
      
    
   
   }

      }
      if (message== 1 ){
    //  digitalWrite(LED+3, HIGH); // LED allumee

       for(i=0;i<NUMPIXELS;i++)
      {
      pixels.setPixelColor(i, pixels.Color(128,128,128)); // Moderately bright red color.
      pixels.show(); // This sends the updated pixel color to the hardware.
      }
      
      
    
      }
       if (message== 2 ){
    //  digitalWrite(LED+3, HIGH); // LED allumee
  
       for(i=0;i<NUMPIXELS;i++)
      {
      pixels.setPixelColor(i, pixels.Color(136,6,206)); // Moderately bright purple color.
      pixels.show(); // This sends the updated pixel color to the hardware.
      }
      
      
    
      }  
  }