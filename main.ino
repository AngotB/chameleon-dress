  #include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif
#define PIN            A2
#define NB             A1
#define BRP            2
#define NUMPIXELS      130
#define STRIPPIXELS    226
#define BRASP          20

#include <SPI.h>
#include <Mirf.h>
#include <nRF24L01.h>
#include <MirfHardwareSpiDriver.h>

Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel strip = Adafruit_NeoPixel(STRIPPIXELS, NB, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel bras = Adafruit_NeoPixel(BRASP, BRP, NEO_GRB + NEO_KHZ800);

byte message;

const int s0 = 3;  
const int s1 = 4;  
const int s2 = 7;  
const int s3 = 6;  
const int out = 5;   

// Variables  
int red = 0;  
int green = 0;  
int blue = 0;
int i=0;
int j=0;
int k=0;
    
void setup()   
{  
  Serial.begin(9600); 
  pinMode(s0, OUTPUT);  
  pinMode(s1, OUTPUT);  
  pinMode(s2, OUTPUT);  
  pinMode(s3, OUTPUT);  
  pinMode(out, INPUT);  
  digitalWrite(s0, HIGH);  
  digitalWrite(s1, LOW);  

  Mirf.cePin = 8; // CE branchee sur 8
  Mirf.csnPin = 9; // CSN branchee sur 9
  Mirf.spi = &MirfHardwareSpi; 
  Mirf.init(); 
  
  Mirf.channel = 0;
  Mirf.payload = sizeof(byte); // taille de ce qu'on va envoyer
  Mirf.config(); 
  
  Mirf.setTADDR((byte *)"nrf02"); // on baptise les deux modules
  Mirf.setRADDR((byte *)"nrf01");
  
 
 #if defined (__AVR_ATtiny85__)
 if (F_CPU == 16000000) clock_prescale_set(clock_div_1);
#endif
pixels.begin();
strip.begin();
bras.begin();
}  
    
void loop() 
{  
  color(); 
  Serial.print("R :");  
  Serial.print(red, DEC);  
  Serial.print(" G : ");  
  Serial.print(green, DEC);  
  Serial.print(" B : ");  
  Serial.print(blue, DEC);  
  //Serial.print("Bl intensity : ");

 message = B00000000;

// Couleur Blanche
 if (red > 20 && red < 30  && green > 20 && green < 30 && blue > 20 && blue < 30)
  {  
   Serial.println(" - (blanc)"); 
   for(i=0;i<NUMPIXELS;i++)
   {
   pixels.setPixelColor(i, pixels.Color(128,128,128));
   pixels.show();
   message = 1;
   }
   
    for(j=0;j<STRIPPIXELS;j++) 
   { 
    strip.setPixelColor(j, strip.Color(128,128,128));
    strip.show();
   }
   for(k=0;k<BRASP;k++) 
   { 
    bras.setPixelColor(k, bras.Color(128,128,128));
    bras.show();
   }
  }  
  
//Couleur violet foncé 
  if (red > 21 && red < 31 && green > 37 && green < 50 && blue > 22 && blue < 37)
  {  
   Serial.println("       - (Violet)"); 
   for(i=0;i<NUMPIXELS;i++)
   {
   pixels.setPixelColor(i, pixels.Color(136,6,206));
   pixels.show();
   message =2;
   }
   
    for(j=0;j<STRIPPIXELS;j++)
   { 
    strip.setPixelColor(j, strip.Color(136,6,206));
    strip.show();
   }
   for(k=0;k<BRASP;k++) 
   { 
    bras.setPixelColor(k, bras.Color(136,6,206));
    bras.show();
   }
  }

  if (red > 4 && red < 8  && green >6 && green <10 && blue > 4 && blue <6)
  {  
  Serial.println(" - (Red Color)"); 
   for(i=0;i<NUMPIXELS;i++)
   {
   pixels.setPixelColor(i, pixels.Color(255,0,0));
   pixels.show();
   message = 3;
   }
   
    for(j=0;j<STRIPPIXELS;j++)
   { 
    strip.setPixelColor(j, strip.Color(255,0,0));
    strip.show();
   }
   for(k=0;k<BRASP;k++) 
   { 
    bras.setPixelColor(k, bras.Color(255,0,0));
    bras.show();
   }
  }  

  else if (red > 16 && red < 22  && green > 17 && green < 23 && blue > 9 && blue < 13)   
  {  
   Serial.println(" - (Blue Color)");
   for(i=0;i<NUMPIXELS;i++)
   {
   pixels.setPixelColor(i, pixels.Color(0,0,255));
   pixels.show();
   message = 4;
   }
   
    for(j=0;j<STRIPPIXELS;j++)
   { 
    strip.setPixelColor(j, strip.Color(0,0,255));
    strip.show();
   }
   for(k=0;k<BRASP;k++) 
   { 
    bras.setPixelColor(k, bras.Color(0,0,255));
    bras.show();
   }
  }  

  else if (red > 6 && red < 9  && green > 6 && green < 8 && blue > 6 && blue < 28)  
  {  
   Serial.println(" - (Green Color)"); 
   for(i=0;i<NUMPIXELS;i++)
   {
   pixels.setPixelColor(i, pixels.Color(0,255,0));
   pixels.show();
   message = 5;
   
   }
   
   for(j=0;j<STRIPPIXELS;j++)
   {
   strip.setPixelColor(j, strip.Color(0,255,0));
   strip.show();
   }
   for(k=0;k<BRASP;k++) 
   { 
    bras.setPixelColor(k, bras.Color(0,255,0));
    bras.show();
   } 
  }  
  Serial.println(); 
  delay(10);
  Mirf.send((byte *)&message);  
  
  while(Mirf.isSending());
  delay(300);

  
 }  
    
void color()  
{    
  digitalWrite(s2, LOW);  
  digitalWrite(s3, LOW);  
  //count OUT, pRed, RED  
  red = pulseIn(out, digitalRead(out) == HIGH ? LOW : HIGH);  
  digitalWrite(s3, HIGH);  
  //count OUT, pBLUE, BLUE  
  blue = pulseIn(out, digitalRead(out) == HIGH ? LOW : HIGH);  
  digitalWrite(s2, HIGH);  
  //count OUT, pGreen, GREEN  
  green = pulseIn(out, digitalRead(out) == HIGH ? LOW : HIGH);  
  digitalWrite(s3, HIGH);
}
